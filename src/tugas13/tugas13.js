import React, {Component} from "react"
import "./Lists.css"

class Lists extends Component{

  constructor(props){
    super(props)
    this.state = {
      dataHargaBuah: [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ],
      inputForm: {
          nama: "",
          harga: "",
          berat: ""
      },
     /// array tidak punya index -1
     indexOfForm: -1    
   }

   this.handleChange = this.handleChange.bind(this);
   this.handleSubmit = this.handleSubmit.bind(this);
   this.handleEdit = this.handleEdit.bind(this);
   this.handleDelete = this.handleDelete.bind(this);
 }
 
 handleDelete(event){
  let index = event.target.value
  let newdataHargaBuah = this.state.dataHargaBuah
  let editedHargaBuah = newdataHargaBuah[this.state.indexOfForm]
  newdataHargaBuah.splice(index, 1)

  if (editedHargaBuah !== undefined){
      // array findIndex baru ada di ES6
      var newIndex = newdataHargaBuah.findIndex((el) => el === editedHargaBuah)
      this.setState({dataHargaBuah: newdataHargaBuah, indexOfForm: newIndex})
      
    }else{

      this.setState({dataHargaBuah: newdataHargaBuah})
    }
    
  }
  
  handleEdit(event){
    let index = event.target.value
    let buah = this.state.dataHargaBuah[index]
    this.setState({
        inputForm: {
            nama: buah.nama,
            harga: buah.harga,
            berat: buah.berat
        },
        indexOfForm: index
    })
  }

  handleChange(event){
    let inputForm = {...this.state.inputForm}
    inputForm[event.target.name] = event.target.value
    // console.log(event.target.value[0]);
    this.setState({
        inputForm
    });
  }

  handleSubmit(event){
    // menahan submit
    event.preventDefault()

    let inputForm = this.state.inputForm

    if (inputForm['nama'].replace(/\s/g,'') !== "" && inputForm['harga'].toString().replace(/\s/g,'') !== "" && inputForm['berat'].toString().replace(/\s/g,'') !== ""){      
      let newdataHargaBuah = this.state.dataHargaBuah
      let index = this.state.indexOfForm
      console.log(index);
      if (index === -1){
        newdataHargaBuah = [...newdataHargaBuah, inputForm]
      }else{
        newdataHargaBuah[index] = inputForm
      }

      this.setState({
        dataHargaBuah: newdataHargaBuah,
        inputForm: {
            nama: "",
            harga: "",
            berat: ""
        },
        indexOfForm: -1
      })
    }else{
        console.log(inputForm)
    }

  }

  render(){
    return(
      <>
      <h1 style={{textAlign: "center"}}>Table Harga Buah</h1>
        <table style={{width: "50%", margin: '0 auto', border: "2px solid black"}}>
          <thead>
            <tr>
            <th id="thhargabuah">No</th>
              <th id="thhargabuah">Nama</th>
              <th id="thhargabuah">Harga</th>
              <th id="thhargabuah">Berat</th>
              <th id="thhargabuah">Action</th>
            </tr>
          </thead>
      {this.state.dataHargaBuah.map((item, index)=>{
        return (

          <tbody>
            <tr key={index}>
              <td>{index+1}</td>  
              <td>{item.nama}</td>
              <td>{item.harga}</td>
              <td>{item.berat/1000 + "kg"}</td>
              <td>
                <button onClick={this.handleEdit} value={index}>Edit</button>
                &nbsp;
                <button onClick={this.handleDelete} value={index}>Delete</button>
              </td>
            </tr>
          </tbody>

          )
        })}
        </table>
      {/* Form */}
      <h1>Form Data Harga Buah</h1>
      <form onSubmit={this.handleSubmit}>
        <label>
        Nama buah:
        </label>          
        <input type="text" name='nama' value={this.state.inputForm.nama} onChange={this.handleChange}/><br/>
        <label>
        Harga buah:
        </label>          
        <input type="text" name='harga' value={this.state.inputForm.harga} onChange={this.handleChange}/><br/>
        <label>
        Berat buah:
        </label>
        <input type="text" name='berat' value={this.state.inputForm.berat} onChange={this.handleChange}/><br/>
        <button>submit</button>
      </form>
      </>
      )
    }
  }

  export default Lists