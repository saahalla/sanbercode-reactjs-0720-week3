import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './tugas15/Routes'
import './App.css';
import Tugas11 from './tugas11/HargaBuah';
import Timer from './tugas12/tugas12'
// import Lists from './tugas13/tugas13'
import Tugas14 from './tugas14/tugas14'

function App() {
  return (
    <Router>
      <Routes />
      {/* masukkan kode anda disini */}
    </Router>
    // <div>
    //   {/* <Tugas11 /> */}
    //   <Timer />
    //   <Tugas14 />
    // </div>
  );
}

export default App;
