import React, {Component} from 'react'

class Timer extends Component{
  constructor(props){
    super(props)
    this.state = {
        date: new Date(),
        time: 100
    };
  }

  componentDidMount(){
    if (this.props.start !== undefined){
      this.setState({time: this.props.start})
    }
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount(){
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date(),
      time: this.state.time - 1
    });
  }


  render(){
    return(
      <>
      {this.state.time >=0 && 
        <table style={{width: "50%", margin: '0 auto'}}>
          <tr>
          <td>
              <h2 style={{textAlign: "left"}}>
              Sekarang jam {this.state.date.toLocaleTimeString()}
              </h2>
          </td>
          <td>
              <h2 style={{textAlign: "right"}}>
              hitung mundur {this.state.time}
              </h2>
          </td>
          </tr>
      </table>
      }
      



      <h1>

      </h1>
      </>
    )
  }
}

export default Timer