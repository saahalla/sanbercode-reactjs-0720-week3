import React, {useContext} from "react"
import {DaftarBuahContext} from "./DaftarBuahContext"

const DaftarBuahList = () =>{
  const [daftarBuah] = useContext(DaftarBuahContext)

  return(
    <>
        <h1 style={{textAlign: "center"}}>Table Harga Buah</h1>
         <table style={{width: "50%", margin: '0 auto', border: "2px solid black"}}>
           <thead>
             <tr>
                <th>No</th>
               <th>Nama</th>
               <th>Harga</th>
               <th>Berat</th>
               <th>Action</th>
             </tr>
           </thead>
            <tbody>
              {
                daftarBuah !== null && daftarBuah.map((item, index)=>{
                  return(                  
                    <tr key={index}>
                      <td>{index+1}</td>
                      <td>{item.nama}</td>
                      <td>{item.harga}</td>
                      <td>{item.berat/1000 + "kg"}</td>
                      <td>
                        <button onClick={handleEdit} value={item.id}>Edit</button>
                        &nbsp;
                        <button onClick={handleDelete} value={item.id}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
              
            </tbody>
        </table>
    </>
  )

}

export default DaftarBuahList