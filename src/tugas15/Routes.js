import React from "react";
import { Switch, Link, Route } from "react-router-dom";
import './routes.css'

// import UserInfo from '../materiHari11/UserInfo';
// import Timer from '../materiHari12/Timer';
// import Lists from '../materiHari13/Lists';
// import HooksLists from '../materiHari14/Lists';
import Tugas11 from '../tugas11/HargaBuah';
import Timer from '../tugas12/tugas12'
import Lists from '../tugas13/tugas13'
import Tugas14 from '../tugas14/tugas14'
import Tugas15 from '../tugas15/DaftarBuah';
// import SimpleHooks from '../materiHari14/SimpleHook';

const Routes = () => {

  return (
    <>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/tugas-11">Tugas 11</Link>
          </li>
          <li>
            <Link to="/tugas-12">Tugas 12</Link>
          </li>
          <li>
            <Link to="/tugas-13">Tugas 13</Link>
          </li>
          <li>
            <Link to="/tugas-14">Tugas 14 </Link>
          </li>
          <li>
            <Link to="/tugas-15">Tugas 15</Link>
          </li>

        </ul>
      </nav>
      <Switch>
        <Route path="/tugas-12" component={Timer}/>
        <Route path="/tugas-13">
          <Lists />
        </Route>
        <Route path="/tugas-14">
          <Tugas14 />
        </Route>
        <Route path="/tugas-15">
          <Tugas15 />
        </Route>
        <Route path="/tugas-11">
          <Tugas11 />
        </Route>
        <Route path="/">
          <Tugas11 />
        </Route>
      </Switch>
    </>
  );
};

export default Routes;
