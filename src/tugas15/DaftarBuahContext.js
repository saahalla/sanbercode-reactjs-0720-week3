import React, { useState, createContext } from "react";

export const DaftarBuahContext = createContext();

export const DaftarBuahProvider = props => {
  const [dataHargaBuah, setdataHargaBuah] = useState(null);

  return (
    <DaftarBuahContext.Provider value={[dataHargaBuah, setdataHargaBuah]}>
      {props.children}
    </DaftarBuahContext.Provider>
  );
};