import React, {useState, useEffect} from "react"
import axios from "axios"
import "./Lists.css"

const Tugas14 = () => {
  const [dataHargaBuah, setdataHargaBuah] =  useState(null) 
  const [getName, setNama] = useState("");
  const [getHarga, setHarga] = useState("");
  const [getBerat, setBerat] = useState("");
  const [selectedId, setSelectedId]  =  useState(0);
  const [statusForm, setStatusForm]  =  useState("create");

  useEffect( () => {
    if (dataHargaBuah === null){
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
      .then(res => {
        console.log(res)
        setdataHargaBuah(res.data.map(el=>{ return {id: el.id, nama: el.name, harga: el.price, berat: el.weight}} ))
      })
    }
  }, [dataHargaBuah])
  
  const handleDelete = (event) => {
    let idBuah = parseInt(event.target.value)

    let newdataBuah = dataHargaBuah.filter(el => el.id != idBuah)

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
    .then(res => {
      console.log(res)
    })
          
    setdataHargaBuah([...newdataBuah])
    
  }
  
  const handleEdit = (event) =>{
    let idBuah = parseInt(event.target.value)
    let buah = dataHargaBuah.find(x=> x.id === idBuah)
    // setinputForm({nama: buah.nama, harga: buah.harga, berat: buah.berat})
    setNama(buah.nama)
    setHarga(buah.harga)
    setBerat(buah.berat)
    setSelectedId(idBuah)
    setStatusForm("edit")
  }

  const handleChangeNama = (event) =>{
    setNama(event.target.value);
  }
  const handleChangeHarga = (event) =>{
    setHarga(event.target.value);
  }
  const handleChangeBerat = (event) =>{
    setBerat(event.target.value);
  }

  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let name = getName
    let price = getHarga
    let weight = getBerat
    
    if (name.replace(/\s/g,'') !== "" && price.replace(/\s/g,'') !== ""){        
      if (statusForm === "create"){        
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight})
        .then(res => {
          setdataHargaBuah([...dataHargaBuah, {id: res.data.id, nama: name, harga: price, berat: weight}])
          console.log(dataHargaBuah);
          console.log(res)
        })
      }else if(statusForm === "edit"){
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${selectedId}`, {name, price, weight})
        .then(res => {
            let HargaBuah = dataHargaBuah.find(el=> el.id === selectedId)
            HargaBuah.nama = name
            HargaBuah.berat = weight
            HargaBuah.harga = price
            setdataHargaBuah([...dataHargaBuah])
        })
      }
      
      setStatusForm("create")
      setSelectedId(0)
      setNama("")
      setHarga("")
      setBerat("")
    }

  }

  return(
    <>
      <h1 style={{textAlign: "center"}}>Table Harga Buah</h1>
         <table style={{width: "50%", margin: '0 auto', border: "2px solid black"}}>
           <thead>
             <tr>
                <th>No</th>
               <th>Nama</th>
               <th>Harga</th>
               <th>Berat</th>
               <th>Action</th>
             </tr>
           </thead>
            <tbody>
              {
                dataHargaBuah !== null && dataHargaBuah.map((item, index)=>{
                  return(                  
                    <tr key={index}>
                      <td>{index+1}</td>
                      <td>{item.nama}</td>
                      <td>{item.harga}</td>
                      <td>{item.berat/1000 + "kg"}</td>
                      <td>
                        <button onClick={handleEdit} value={item.id}>Edit</button>
                        &nbsp;
                        <button onClick={handleDelete} value={item.id}>Delete</button>
                      </td>
                    </tr>
                  )
                })
              }
              
            </tbody>
        </table>
      {/* Form */}
      <div style={{width: "50%", margin: "0 auto", display: "block"}}>
        <div style={{padding: "20px"}}>
          <h1>Form Data Harga Buah</h1>
          <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
            Nama buah:
            </label>          
            <input style={{float: "right"}} type="text" name='nama' value={getName} onChange={handleChangeNama}/><br/>
            <label style={{float: "left"}}>
            Harga buah:
            </label>          
            <input style={{float: "right"}} type="text" name='harga' value={getHarga} onChange={handleChangeHarga}/><br/>
            <label style={{float: "left"}}l>
            Berat buah:
            </label>
            <input style={{float: "right"}} type="text" name='berat' value={getBerat} onChange={handleChangeBerat}/><br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button style={{ float: "right"}}>submit</button>
            </div>
          </form>
        </div>
      </div>
      
    </>
  )

}
  export default Tugas14